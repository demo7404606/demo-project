<?php
/**
 * Template Name: Courses
 */
get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <?php
            $page = isset($_GET['course_page']) ? max(1, (int)$_GET['course_page']) : 1;
            $url = 'https://service.fetchcourses.ie/service/fetchcourse.svc/json/SearchCourseListSummaryAdvanced/' . $page . '/10';
            $headers = array(
                'Content-Type' => 'application/json',
                'Cookie' => 'ASP.NET_SessionId=1ug15masbnmn1rd4xxzocvse',
            );

            $body = json_encode(array(
                'ISCEDIds' => 0,
                'ProviderIds' => 13,
                'Keywords' => '',
                'LocationIds' => 0,
                'DeliveryModeId' => 0
            ));

            $response = wp_remote_post($url, array(
                'headers' => $headers,
                'body' => $body,
            ));

            if (is_wp_error($response)) {
                // Handle error
                echo "<div class='errorResponse'>Error: " . $response->get_error_message() . "</div>";
            } else {
                // Successful response
                $body = wp_remote_retrieve_body($response);
                $data = json_decode($body, true);

                $data = $data['SearchCourseListSummaryAdvancedResult'];
                $courses = $data['courses'];
                $pages = $data['numberOfPages'];

                if (count($courses) > 0) { ?>
                    <div class="coursesWrapper row"><?php
                        foreach ($data['courses'] as $course){ ?>
                            <div class="col-sm-4">
                                <a href="https://www.fetchcourses.ie/course/finder?sfcw-courseId=<?= $course['CourseId'] ?>">
                                    <div class="card">
                                        <div class="image">
                                            <img src="http://loremflickr.com/320/150?random=<?= rand(1,10) ?>" />
                                        </div>
                                        <div class="card-inner">
                                            <div class="header">
                                                <h2><?= $course['CourseTitle'] ?></h2>
                                                <h3><?= $course['CategoryDescription'] ?></h3>
                                            </div>
                                            <div class="content">
                                                <p><strong>Centre:</strong> <?= $course['CentreName'] ?></p>
                                                <p><strong>Capacity:</strong> <?= $course['CourseCapacity'] ?></p>
                                                <p><strong>Code:</strong> <?= $course['CourseCode'] ?></p>
                                                <p><strong>Contact Email:</strong> <?= $course['CourseContactEmail'] ?></p>
                                                <p><strong>Contact Name:</strong> <?= $course['CourseContactName'] ?></p>
                                                <p><strong>Location:</strong> <?= $course['CourseLocation'] ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </a>

                            </div><?php
                        } ?>
                    </div><?php
                }

                if( $pages > 1 ) {
                    echo paginate_links(
                        array(
                            'total' => $pages,
                            'current' => $data['pageCurrent'],
                            'base' => get_permalink() . '%_%',
                            'format' => '?course_page=%#%'
                        )
                    );
                }
            }
            ?>

        </main><!-- .site-main -->

    </div><!-- .content-area -->

<?php get_footer(); ?>
<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://demoplugin.com
 * @since      1.0.0
 *
 * @package    Demo_Plugin
 * @subpackage Demo_Plugin/public/partials
 */
?>
<div class="registrationFormWrapper">
    <form id="memberRegistrationForm">
        <div class="form-group">
            <label for="first_name">First Name:</label>
            <input class="form-control" type="text" name="first_name" id="first_name" required>
        </div>

        <div class="form-group">
            <label for="last_name">Last Name:</label>
            <input class="form-control" type="text" name="last_name" id="last_name" required>
        </div>

        <div class="form-group">
            <label for="email">Email:</label>
            <input class="form-control" type="email" name="email" id="email" required>
        </div>

        <div class="form-group">
            <label for="phone">Phone:</label>
            <input class="form-control" type="tel" name="phone" id="phone" required>
        </div>

        <div class="form-group">
            <div id="card-element"></div>
        </div>

        <div id="payment-result"></div>
        <input id="memberRegistrationBtn" class="btn btn-primary" type="submit" value="Register">
    </form>
</div>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->

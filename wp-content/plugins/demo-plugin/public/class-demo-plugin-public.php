<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://demoplugin.com
 * @since      1.0.0
 *
 * @package    Demo_Plugin
 * @subpackage Demo_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Demo_Plugin
 * @subpackage Demo_Plugin/public
 * @author     Demo <demo@gmail.com>
 */
class Demo_Plugin_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Demo_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Demo_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/demo-plugin-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Demo_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Demo_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

        wp_enqueue_script('stripe', 'https://js.stripe.com/v3/', array('jquery'), null, true);
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/demo-plugin-public.js', array( 'jquery', 'stripe' ), $this->version, [ 'in_footer' => true ] );
        wp_localize_script( $this->plugin_name, 'settings', array(
            'key' => get_option( 'demo-plugin-stripe-key' ),
            'ajax_url' => admin_url( 'admin-ajax.php' )
        ) );

	}

    /**
     * Display the registration form content.
     *
     * @since    1.0.0
     */
    public function display_registration_form() {

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/demo-plugin-public-display.php';

    }

    /**
     * Process member registration.
     *
     * @since    1.0.0
     */
    public function process_member_registration() {

        $secret_key = get_option('demo-plugin-stripe-secret-key');
        \Stripe\Stripe::setApiKey($secret_key);

        if (isset($_POST['payment_method_id'])) {
            $payment_method_id = sanitize_text_field($_POST['payment_method_id']);
            $form_data = wp_unslash($_POST['form_data']);
            parse_str($form_data, $form_fields);
            $amount = 10000; // $100 in cents

            try {
                $paymentIntent = \Stripe\PaymentIntent::create([
                    'payment_method' => $payment_method_id,
                    'amount' => $amount,
                    'currency' => 'usd',
                    'confirmation_method' => 'manual',
                    'confirm' => true,
                    "return_url"=> "http://localhost/demo-project/success-page"
                ]);

                $payment_data = array(
                    'first_name' => sanitize_text_field($form_fields['first_name']),
                    'last_name' => sanitize_text_field($form_fields['last_name']),
                    'email' => sanitize_email($form_fields['email']),
                    'phone' => sanitize_text_field($form_fields['phone']),
                    'amount' => $amount,
                    'stripe_payment_intent_id' => $paymentIntent->id,
                );

                $payment_id = wp_insert_post(array(
                    'post_type' => 'payment_history',
                    'post_title' => $payment_data['first_name'] . ' ' . $payment_data['last_name'],
                    'post_status' => 'publish',
                ));

                foreach ($payment_data as $key => $value) {
                    update_post_meta($payment_id, $key, $value);
                }

                wp_send_json_success(['message' => 'Payment successful!']);
            } catch (\Exception $e) {
                wp_send_json_error(['message' => 'Error: ' . $e->getMessage()]);
            }
        } else {
            wp_send_json_error(['message' => 'Invalid request.']);
        }

    }

}

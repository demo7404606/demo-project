(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	let stripe = Stripe(settings.key);
	let elements = stripe.elements();

	let style = {
		base: {
			fontSize: '16px',
			color: '#32325d',
		}
	};

	let card = elements.create('card', { style: style });
	card.mount('#card-element');

	$('#memberRegistrationForm').submit(function(e) {
		e.preventDefault();
		let form = $(this);
		stripe.createPaymentMethod({
			type: 'card',
			card: card,
		}).then(function(result) {
			if (result.error) {
				// Display error message
				$('#payment-result').html('<p>Error: ' + result.error.message + '</p>');
			} else {
				// Handle payment and registration on the server
				var paymentMethodId = result.paymentMethod.id;
				var formData = form.serialize();

				$.ajax({
					url: settings.ajax_url,
					type: 'post',
					data: {
						action: 'process_member_registration',
						payment_method_id: paymentMethodId,
						form_data: formData,
					},
					success: function(response) {
						if (response.success) {
							$('#payment-result').html('<p class="alert alert-success">' + response.data.message + '</p>');
						} else {
							$('#payment-result').html('<p class="alert alert-danger">' + response.data.message + '</p>');
						}
					}
				});
			}
		});
	});

})( jQuery );

<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://demoplugin.com
 * @since      1.0.0
 *
 * @package    Demo_Plugin
 * @subpackage Demo_Plugin/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Demo_Plugin
 * @subpackage Demo_Plugin/admin
 * @author     Demo <demo@gmail.com>
 */
class Demo_Plugin_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles($hook) {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Demo_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Demo_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
        if ( ! ( 'toplevel_page_demo-plugin-settings' === $hook ) ) {
            return;
        }

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/demo-plugin-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts( $hook ) {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Demo_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Demo_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
        if ( ! ( 'toplevel_page_demo-plugin-settings' === $hook ) ) {
            return;
        }

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/demo-plugin-admin.js', array( 'jquery' ), $this->version, false );

	}

    /**
     * Register the settings page for the admin area.
     *
     * @since    1.0.0
     */
    public function register_settings_page() {
        add_menu_page(
            __('Demo Plugin', 'demo-plugin'),
            __('Stripe Setting', 'demo-plugin'),
            'manage_options',
            'demo-plugin-settings',
            array($this, 'display_settings_page')
        );
    }

    /**
     * Display the settings page content.
     *
     * @since    1.0.0
     */
    public function display_settings_page() {

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/demo-plugin-admin-settings.php';

    }

    /**
     * Register the settings for our settings page.
     *
     * @since    1.0.0
     */
    public function register_settings() {

        // Here we are going to register our setting.
        register_setting(
            $this->plugin_name . '-settings-group',
            $this->plugin_name . '-stripe-key',
            array(
                'sanitize_callback' => 'sanitize_text_field'
            )
        );

        register_setting(
            $this->plugin_name . '-settings-group',
            $this->plugin_name . '-stripe-secret-key',
            array(
                'sanitize_callback' => 'sanitize_text_field'
            )
        );

        // Here we are going to add a section for our setting.
        add_settings_section(
            $this->plugin_name . '-settings-section',
            __( 'Demo Plugin Settings', 'demo-plugin' ),
            array( $this, 'demo_plugin_add_settings_section' ),
            $this->plugin_name . '-settings'
        );

        // Here we are going to add fields to our section.
        add_settings_field(
            $this->plugin_name . '-stripe-key',
            __( 'Stripe Publishable Key', 'demo-plugin' ),
            array( $this, 'demo_plugin_add_settings_field_input_pub_key' ),
            $this->plugin_name . '-settings',
            $this->plugin_name . '-settings-section',
            array(
                'label_for' => $this->plugin_name . '-stripe-key'
            )
        );

        add_settings_field(
            $this->plugin_name . '-stripe-secret-key',
            __( 'Stripe Secret Key', 'demo-plugin' ),
            array( $this, 'demo_plugin_add_settings_field_input_secret_key' ),
            $this->plugin_name . '-settings',
            $this->plugin_name . '-settings-section',
            array(
                'label_for' => $this->plugin_name . '-stripe-secret-key'
            )
        );

    }

    /**
     * Callback function for adding settings section.
     *
     * @since    1.0.0
     */
    function demo_plugin_add_settings_section() {

    }

    /**
     * Callback function for adding stripe publishable key field for settings section.
     *
     * @since    1.0.0
     */
    function demo_plugin_add_settings_field_input_pub_key() {
        $value = get_option( 'demo-plugin-stripe-key' );
        echo '<input type="text" name="demo-plugin-stripe-key" id="demo-plugin-stripe-key" class="regular-text" value="' . esc_attr($value) . '" />';
    }

    /**
     * Callback function for adding stripe secret key field for settings section.
     *
     * @since    1.0.0
     */
    function demo_plugin_add_settings_field_input_secret_key() {
        $value = get_option( 'demo-plugin-stripe-secret-key' );
        echo '<input type="text" name="demo-plugin-stripe-secret-key" id="demo-plugin-stripe-secret-key" class="regular-text" value="' . esc_attr($value) . '" />';
    }

    /**
     * Function for registering Payment History.
     *
     * @since    1.0.0
     */
    function register_payment_history_post_type() {
        $labels = array(
            'name'               => _x('Payment History', 'post type general name', 'demo-plugin'),
            'singular_name'      => _x('Payment History', 'post type singular name', 'demo-plugin'),
            'menu_name'          => _x('Payment History', 'admin menu', 'demo-plugin'),
            'name_admin_bar'     => _x('Payment History', 'add new on admin bar', 'demo-plugin'),
            'add_new'            => _x('Add New', 'payment history', 'demo-plugin'),
            'add_new_item'       => __('Add New Payment', 'demo-plugin'),
            'new_item'           => __('New Payment', 'demo-plugin'),
            'edit_item'          => __('Edit Payment', 'demo-plugin'),
            'view_item'          => __('View Payment', 'demo-plugin'),
            'all_items'          => __('All Payments', 'demo-plugin'),
            'search_items'       => __('Search Payments', 'demo-plugin'),
            'not_found'          => __('No payments found', 'demo-plugin'),
            'not_found_in_trash' => __('No payments found in trash', 'demo-plugin'),
        );

        $args = array(
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array('slug' => 'payment_history'),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array('title', 'editor', 'author', 'thumbnail'),
        );

        register_post_type('payment_history', $args);
    }

    /**
     * Add columns for Payment History information.
     *
     * @since    1.0.0
     */
    function add_extra_columns_for_payment_history($columns) {
        return [
            'title' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'payment_amount' => 'Amount',
            'stripe_pi_id' => 'Payment ID',
            'date' => 'Date'
        ];
    }

    /**
     * Populate extra columns data.
     *
     * @since    1.0.0
     */
    function show_extra_columns_data($column, $post_id) {
        if ($column == 'payment_amount') {
            $cents = get_post_meta($post_id, 'amount', true);
            echo '$' . $cents/100;
        }

        if ($column == 'email') {
            echo get_post_meta($post_id, 'email', true);
        }

        if ($column == 'phone') {
            echo get_post_meta($post_id, 'phone', true);
        }

        if ($column == 'stripe_pi_id') {
            echo get_post_meta($post_id, 'stripe_payment_intent_id', true);
        }
    }

}

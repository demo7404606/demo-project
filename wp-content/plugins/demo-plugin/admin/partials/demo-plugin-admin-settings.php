<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://demoplugin.com
 * @since      1.0.0
 *
 * @package    Demo_Plugin
 * @subpackage Demo_Plugin/admin/partials
 */
?>

<div id="wrap">
    <form method="post" action="options.php">
        <?php
        settings_fields( 'demo-plugin-settings-group' );
        do_settings_sections( 'demo-plugin-settings' );
        submit_button();
        ?>
    </form>
</div>